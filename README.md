Create .env.local from .env file

Up and build: docker container docker-compose up -d --build
If problems with build: sudo chmod -R 777 project_dir/var/db
Enter to php container: docker exec -it php_app bash
Composer: composer install
Create .env.local from .env file

Up database with migrations
php bin/console doctrine:migrations:migrate


You can test project in postman. Collection for postman in public/doc

chmod -R 777 /var/www/html/public/
