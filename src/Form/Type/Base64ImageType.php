<?php
declare(strict_types=1);

namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Base64ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function ($file) {
                // Преобразование не требуется при загрузке данных из модели
                return $file;
            },
            function ($base64String) {
                // Преобразование Base64 строки в UploadedFile
                if ($base64String) {
                    // Декодирование строки
                    $decoded = base64_decode($base64String);
                    // Создание временного файла
                    $tempPath = tempnam(sys_get_temp_dir(), 'upl');
                    file_put_contents($tempPath, $decoded);

                    // Создание объекта UploadedFile
                    return new UploadedFile($tempPath, 'filename.jpg', 'image/jpeg', null, true);
                }

                return null;
            }
        ));
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
