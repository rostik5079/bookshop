<?php
declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Form\FormInterface;

class FormErrorChecker
{
    public function checkForm(FormInterface $form, $data): array
    {
        $form->submit($data);
        $errorMessages = [];
        if (!$form->isValid()) {
            foreach ($form->getErrors(true) as $error) {
                $errorMessages[] = $error->getMessage();
            }
        }
        return $errorMessages;
    }
}
