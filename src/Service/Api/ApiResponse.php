<?php
declare(strict_types=1);

namespace App\Service\Api;

use Symfony\Component\HttpFoundation\Response;
class ApiResponse
{
    private mixed $data;
    private int $statusCode;
    private array $errors;

    public function __construct($data = null, int $statusCode = Response::HTTP_OK, array $errors = [])
    {
        $this->data = $data;
        $this->statusCode = $statusCode;
        $this->errors = $errors;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
