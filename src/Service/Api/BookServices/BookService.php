<?php
declare(strict_types=1);

namespace App\Service\Api\BookServices;

use App\Entity\Author;
use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;
use App\Service\Api\ApiResponse;
use App\Service\FormErrorChecker;
use App\Service\PaginationService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Vich\UploaderBundle\VichUploaderBundle;

class BookService
{
    public function __construct(
        private readonly BookRepository         $bookRepository,
        private readonly SerializerInterface    $serializer,
        private readonly ValidatorInterface     $validator,
        private readonly EntityManagerInterface $entityManager,
        private readonly FormFactoryInterface   $formFactory,
        private readonly PaginationService      $paginationService,
        private readonly FormErrorChecker       $formErrorChecker
    )
    {
    }

    public function list(int $page, int $limit): ApiResponse
    {
        $queryBuilder = $this->bookRepository->createQueryBuilder('a');

        $pagination = $this->paginationService->paginate($queryBuilder, $page, $limit);
        $books = $pagination->getItems();

        $serializedBooks = $this->serializer->serialize($books, 'json', ['groups' => ['api']]);

        return new ApiResponse(
            [
                'books' => $serializedBooks,
                'pagination' => [
                    'totalItems' => $pagination->getTotalItemCount(),
                    'itemsPerPage' => $pagination->getItemNumberPerPage(),
                    'currentPage' => $pagination->getCurrentPageNumber(),
                ],
            ],
            Response::HTTP_OK
        );
    }

    public function create(Request $request): ApiResponse
    {
        $data = $request->getPayload()->all();
        $form = $this->formFactory->create(BookType::class);
        $errorMessages = $this->formErrorChecker->checkForm($form, $data);

        if (!empty($errorMessages)) {
            return new ApiResponse(null, Response::HTTP_BAD_REQUEST, $errorMessages);
        }

        /** @var Book $book */
        $book = $form->getData();
        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return new ApiResponse("Book created successful", Response::HTTP_CREATED);
    }

    public function getBookData(Book $book): string
    {
        return $this->serializer->serialize($book, 'json', ['groups' => ['api']]);
    }

    /**
     * @throws Exception
     */
    public function validateBookData(array $data): array
    {
        $book = new Book();
        $book->setName($data['name'] ?? '');
        $book->setShortDescription($data['shortDescription'] ?? '');
        $book->setPublicationDate(new DateTime($data['publicationDate'] ?? 'now'));
        $book->setImageName($data['imageName'] ?? '');

        if (isset($data['authors']) && is_array($data['authors'])) {
            $authors = $this->entityManager->getRepository(Author::class)->findBy(['id' => $data['authors']]);
            foreach ($authors as $author) {
                $book->addAuthor($author);
            }
        }

        $errors = $this->validator->validate($book);

        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[] = $error->getMessage();
        }

        return $errorMessages;
    }

    /**
     * @throws Exception
     */
    public function updateBookData(Book $book, array $data): void
    {
        $book->setName($data['name'] ?? $book->getName());
        $book->setShortDescription($data['shortDescription'] ?? $book->getShortDescription());
        $book->setPublicationDate(new DateTime($data['publicationDate'] ?? 'now'));
        $book->setImageName($data['imageName'] ?? '');

        if (isset($data['authors']) && is_array($data['authors'])) {
            $existingAuthors = $book->getAuthors()->toArray();

            foreach ($existingAuthors as $author) {
                if (!in_array($author->getId(), $data['authors'])) {
                    $book->removeAuthor($author);
                }
            }

            $newAuthors = $this->entityManager->getRepository(Author::class)->findBy(['id' => $data['authors']]);
            foreach ($newAuthors as $author) {
                if (!$book->getAuthors()->contains($author)) {
                    $book->addAuthor($author);
                }
            }
        }
    }

    public function findByAuthor(int $authorId): string
    {
        $books = $this->bookRepository->findByAuthor($authorId);

        return $this->serializer->serialize($books, 'json', ['groups' => ['api']]);
    }
}
