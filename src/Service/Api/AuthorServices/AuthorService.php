<?php
declare(strict_types=1);

namespace App\Service\Api\AuthorServices;

use App\Entity\Author;
use App\Form\AuthorType;
use App\Repository\AuthorRepository;
use App\Service\Api\ApiResponse;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthorService
{
    public function __construct(
        private readonly AuthorRepository       $authorRepository,
        private readonly SerializerInterface    $serializer,
        private readonly ValidatorInterface     $validator,
        private readonly EntityManagerInterface $entityManager,
        private readonly FormFactoryInterface   $formFactory,
        private readonly PaginationService      $paginationService
    )
    {
    }

    public function list(int $page, int $limit): ApiResponse
    {
        $queryBuilder = $this->authorRepository->createQueryBuilder('a');

        $pagination = $this->paginationService->paginate($queryBuilder, $page, $limit);
        $authors = $pagination->getItems();

        $serializedAuthors = $this->serializer->serialize($authors, 'json');

        return new ApiResponse(
            [
                'authors' => $serializedAuthors,
                'pagination' => [
                    'totalItems' => $pagination->getTotalItemCount(),
                    'itemsPerPage' => $pagination->getItemNumberPerPage(),
                    'currentPage' => $pagination->getCurrentPageNumber(),
                ],
            ],
            Response::HTTP_OK
        );
    }

    public function create(Request $request): ApiResponse
    {
        $data = $request->getPayload()->all();
        $form = $this->formFactory->create(AuthorType::class);
        $form->submit($data);

        if (!$form->isValid()) {
            $errorMessages = [];
            foreach ($form->getErrors(true) as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return new ApiResponse(null, Response::HTTP_BAD_REQUEST, $errorMessages);
        }

        /** @var Author $author */
        $author = $form->getData();
        $this->entityManager->persist($author);
        $this->entityManager->flush();

        $serializedAuthor = $this->serializer->serialize($author, 'json');
        return new ApiResponse($serializedAuthor, Response::HTTP_CREATED);
    }

    public function getAuthorData(Author $author): array
    {
        return $this->serializer->normalize($author);
    }

    public function validateAuthorData(array $data): array
    {
        $author = new Author();
        $author->setName($data['name'] ?? '');
        $author->setLastName($data['lastName'] ?? '');
        $author->setThirdName($data['thirdName'] ?? '');

        $errors = $this->validator->validate($author);

        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[] = $error->getMessage();
        }

        return $errorMessages;
    }

    public function updateAuthorData(Author $author, array $data): void
    {
        $author->setName($data['name'] ?? $author->getName());
        $author->setLastName($data['lastName'] ?? $author->getLastName());
        $author->setThirdName($data['thirdName'] ?? $author->getThirdName());
    }
}
