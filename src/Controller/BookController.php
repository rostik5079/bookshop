<?php

namespace App\Controller;

use App\Entity\Book;
use App\Service\Api\BookServices\BookService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends AbstractController
{
    public function __construct(private readonly BookService $bookService)
    {
    }

    public function list(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);

        $response = $this->bookService->list($page, $limit);

        return $this->json($response->getData(), $response->getStatusCode());
    }

    public function create(Request $request): JsonResponse
    {
        $apiResponse = $this->bookService->create($request);

        if ($apiResponse->getData() === null) {
            return $this->json(['errors' => $apiResponse->getErrors()], $apiResponse->getStatusCode());
        }

        return $this->json($apiResponse->getData(), $apiResponse->getStatusCode());
    }

    public function show(Book $book): JsonResponse
    {
        $bookData = $this->bookService->getBookData($book);

        if (!$bookData) {
            return $this->json(['error' => 'Book not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($bookData, Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    public function edit(Request $request, Book $book, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = $request->getPayload()->all();

        $errors = $this->bookService->validateBookData($data);

        if (count($errors) > 0) {
            return $this->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $this->bookService->updateBookData($book, $data);
        $entityManager->flush();

        return $this->json(['message' => 'Book updated successfully'], Response::HTTP_OK);
    }

    public function findByAuthorAction(int $authorId): JsonResponse
    {
        return $this->json($this->bookService->findByAuthor($authorId), Response::HTTP_OK);
    }

//    public function delete(Request $request, Book $book, EntityManagerInterface $entityManager): Response
//    {
//        if ($this->isCsrfTokenValid('delete'.$book->getId(), $request->request->get('_token'))) {
//            $entityManager->remove($book);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('app_book_index', [], Response::HTTP_SEE_OTHER);
//    }
}
