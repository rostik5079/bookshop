<?php

namespace App\Controller;

use App\Entity\Author;
use App\Service\Api\AuthorServices\AuthorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorController extends AbstractController
{
    public function __construct(private readonly AuthorService $authorService)
    {
    }

    public function list(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);

        $response = $this->authorService->list($page, $limit);

        return $this->json($response->getData(), $response->getStatusCode());
    }

    public function create(Request $request): JsonResponse
    {
        $apiResponse = $this->authorService->create($request);

        if ($apiResponse->getData() === null) {
            return new JsonResponse(['errors' => $apiResponse->getErrors()], $apiResponse->getStatusCode());
        }

        return new JsonResponse(
            $apiResponse->getData(),
            $apiResponse->getStatusCode(),
            [],
            true
        );
    }

    public function show(Author $author): JsonResponse
    {
        $authorData = $this->authorService->getAuthorData($author);

        if (empty($authorData)) {
            return new JsonResponse(['error' => 'Author not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($authorData, Response::HTTP_OK);
    }

    public function edit(Request $request, Author $author, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = $request->getPayload()->all();

        $errors = $this->authorService->validateAuthorData($data);

        if (count($errors) > 0) {
            return new JsonResponse(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $this->authorService->updateAuthorData($author, $data);
        $entityManager->flush();

        return new JsonResponse(['message' => 'Author updated successfully'], Response::HTTP_OK);
    }

//    Maybe later, need more time for realization this method, because cascade delete for books needed =)

//    public function delete(Author $author, EntityManagerInterface $entityManager): JsonResponse
//    {
//        $entityManager->remove($author);
//        $entityManager->flush();
//
//        return new JsonResponse(['message' => 'Author deleted successfully'], Response::HTTP_OK);
//    }
}
